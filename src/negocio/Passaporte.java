package negocio;

public class Passaporte {
    private int codigo = 0;
    private String nome = "";
    private String sexo = "";
    private Pais objPais = null;

    public Passaporte() {
    }

    public Passaporte(int codigo, String nome, String sexo, Pais objPais) {
        this.codigo = codigo;
        this.nome = nome;
        this.sexo = sexo;
        this.objPais = objPais;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Pais getObjPais() {
        return objPais;
    }

    public void setObjPais(Pais objPais) {
        this.objPais = objPais;
    }
}
