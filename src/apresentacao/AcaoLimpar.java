package apresentacao;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class AcaoLimpar implements ActionListener {
    private JTextField txtCodigo = null;
    private JTextField txaNome = null;
    private JComboBox<String> cboPais = null;
    private JRadioButton radioSexo = null;

    public AcaoLimpar(JTextField txtCodigo, JTextField txaNome, JComboBox<String> cboPais, JRadioButton radioSexo) {
        this.txtCodigo = txtCodigo;
        this.txaNome = txaNome;
        this.cboPais = cboPais;
        this.radioSexo = radioSexo;
    }

    public void actionPerformed(ActionEvent e) {
        txtCodigo.setText("");
        txaNome.setText("");
        cboPais.setSelectedIndex(0);
        radioSexo.setSelected(false);
    }
}