package apresentacao;

import javax.swing.*;

public class Main  extends JFrame {

    private static final long serialVersionUID = 1L;

    private JPanel objPainel = new JPanel();

    private JLabel lblCodigo = new JLabel("Numero");
    private JTextField txtCodigo = new JTextField();

    private JLabel lblNome = new JLabel("Nome");
    private JTextField txtNome = new JTextField();

    private JRadioButton radioMasculino = new JRadioButton("Masculino");
    private JRadioButton radioFeminino = new JRadioButton("Feminino");

    private JLabel lblPais = new JLabel("Pais");
    private JComboBox<String> cboPais = new JComboBox<String>();

    private JButton btnGravar = new JButton("Gravar");
    private JButton btnLimpar = new JButton("Limpar");
    private JButton btnSair = new JButton("Sair");

    public static void main(String[] args) {
            new Main().setVisible(true);
    }

    public Main() {
        setTitle("Cadastro");
        setSize(640, 480);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        objPainel.setLayout(null);
        setContentPane(objPainel);

        lblCodigo.setBounds(20, 20, 200, 20);
        objPainel.add(lblCodigo);

        txtCodigo.setBounds(20, 40, 50, 20);
        objPainel.add(txtCodigo);


        lblNome.setBounds(20, 60, 200, 20);
        objPainel.add(lblNome);

        txtNome.setBounds(20, 80, 100, 20);
        objPainel.add(txtNome);

        lblPais.setBounds(20, 100, 200, 20);
        objPainel.add(lblPais);

        ButtonGroup group = new ButtonGroup();
        group.add(radioMasculino);
        group.add(radioFeminino);

        radioMasculino.setBounds(17, 120, 200, 20);
        objPainel.add(radioMasculino);

        radioFeminino.setBounds(100, 120, 200, 20);
        objPainel.add(radioFeminino);

        lblPais.setBounds(20, 160, 200, 20);
        objPainel.add(lblPais);

        cboPais.setBounds(20, 180, 150, 20);
        cboPais.addItem("");
        cboPais.addItem("Brasil");
        cboPais.addItem("EUA");
        cboPais.addItem("FRANCA");

        objPainel.add(cboPais);

        btnGravar.setBounds(300, 400, 100, 30);
        btnGravar.addActionListener(new AcaoGravar(txtCodigo,
                txtNome,
                cboPais,
                radioMasculino));
        objPainel.add(btnGravar);

        btnLimpar.setBounds(410, 400, 100, 30);
        btnLimpar.addActionListener(new AcaoLimpar(txtCodigo,
                txtNome,
                cboPais,
                radioMasculino));
        objPainel.add(btnLimpar);

        btnSair.setBounds(520, 400, 100, 30);
        btnSair.addActionListener(new AcaoSair());
        objPainel.add(btnSair);
    }
}
