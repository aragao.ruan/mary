package apresentacao;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


import negocio.Pais;
import negocio.Passaporte;


public class AcaoGravar implements ActionListener {
    private JTextField txtCodigo = null;
    private JTextField txaNome = null;
    private JComboBox<String> cboPais = null;
    private JRadioButton radioSexo = null;

    public AcaoGravar(JTextField txtCodigo, JTextField txaNome, JComboBox<String> cboOPais, JRadioButton radioSexo) {
        this.txtCodigo = txtCodigo;
        this.txaNome = txaNome;
        this.cboPais = cboOPais;
        this.radioSexo = radioSexo;
    }

    public void actionPerformed(ActionEvent evento) {
        Passaporte objPassaporte = new Passaporte();

        try {
            objPassaporte.setCodigo(Integer.parseInt(txtCodigo.getText()));
        } catch (Exception erro) {
            JOptionPane.showMessageDialog(null,
                    "Msg Error");
            return;
        }

        if (txaNome.getText().equals("")) {
            JOptionPane.showMessageDialog(null,
                    "Msg Error");
            return;
        }

        if (cboPais.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(null,
                    "Msg Error");
            return;
        }

        objPassaporte.setObjPais(new Pais(cboPais.getSelectedItem().toString()));

        JOptionPane.showMessageDialog(null, "Gravação Realizada com Sucesso !");
    }
}